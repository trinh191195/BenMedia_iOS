//
//  Product+CoreDataProperties.swift
//  
//
//  Created by Đỗ Ngọc Trình on 1/27/18.
//
//

import Foundation
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }

    @NSManaged public var info: String?
    @NSManaged public var urlImage: String?
    @NSManaged public var urlThumnail: String?

}
