//
//  PreviewVC.swift
//  BenMedia
//
//  Created by Đỗ Ngọc Trình on 1/21/18.
//  Copyright © 2018 Đỗ Ngọc Trình. All rights reserved.
//

import UIKit

class PreviewVC: UIViewController {
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var imvPreview: UIImageView!
    var imgURL : String!
    var listProduct = [Media]()
    var startIndex = -1
    
    @IBAction func prevTap(_ sender: Any) {
        startIndex = startIndex - 1
        if(startIndex < 0){
            startIndex = listProduct.count - 1
        }
        startIndex = startIndex % listProduct.count
        loadImage()
    }
    @IBAction func nextTap(_ sender: Any) {
        startIndex = startIndex + 1
        if(startIndex >= listProduct.count){
            startIndex = 0
        }
        loadImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.modalTransitionStyle = .crossDissolve
        loadImage()
    }
    
    func loadImage() {
        if(startIndex >= 0 && startIndex < listProduct.count){
            let selectItem = listProduct[startIndex]
            imvPreview.sd_setImage(with: URL(string:selectItem.urlImage))
            let indexString = "\(startIndex + 1)"
            let totalString =  "\(listProduct.count)"
            lblIndex.attributedText = attributedString(from:"\(indexString)/\(totalString)", nonBoldRange: NSMakeRange(indexString.count, totalString.count+1))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //reset image
        //  imvPreview.image = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnDismiss(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
