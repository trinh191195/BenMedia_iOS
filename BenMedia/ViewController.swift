//
//  ViewController.swift
//  BenMedia
//
//  Created by Đỗ Ngọc Trình on 1/15/18.
//  Copyright © 2018 Đỗ Ngọc Trình. All rights reserved.
//

import UIKit
import ImageIO
import SDWebImage

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return  clvImages.frame.width/3*1.25
    }
    
    @IBOutlet weak var clvImages: UICollectionView!
    var refresher : UIRefreshControl!
    let cellId = "cellId"
    let numberCollum = 3
    var listProduct = [Media]()
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresher = UIRefreshControl()
        clvImages.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        clvImages.addSubview(refresher)
        clvImages.register(UINib(nibName:String(describing: ImageCell.self),bundle: nil), forCellWithReuseIdentifier: cellId)
        
        clvImages.setCollectionViewLayout(PinterestLayout(), animated: true)
        // Set the PinterestLayout delegate
        if let layout = clvImages?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        clvImages?.backgroundColor = UIColor.clear
        clvImages?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)
        
        let longPressToView = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        self.view.addGestureRecognizer(longPressToView)

        APIManager.getJSON(url: "https://dl.dropboxusercontent.com/s/r0h6az3m9tftjow/benMedia.json?dl=0") { (result) in
            if result !=  nil {
                self.listProduct = result!
                self.clvImages.reloadData()
            }
        }
    }
    
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer){  
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProduct.count
    }
    
    @objc func loadData(){
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ImageCell
        cell.imvContent.sd_setImage(with: URL(string: listProduct[indexPath.row].urlImage), placeholderImage: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.modalTransitionStyle = .crossDissolve
        let previewVC = storyboard!.instantiateViewController(withIdentifier: String(describing: PreviewVC.self)) as? PreviewVC
        previewVC?.imgURL = listProduct[indexPath.row].urlImage
        previewVC?.listProduct = listProduct
        previewVC?.startIndex = indexPath.row
        present(previewVC!, animated: true)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            self.clvImages.reloadData()
            switch orient {
            case .portrait:
                print("Portrait")
            // Do something
            default:
                print("Anything But Portrait")
                // Do something else
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            print("rotation completed")
        })
        super.viewWillTransition(to: size, with: coordinator)
    }
    
}

